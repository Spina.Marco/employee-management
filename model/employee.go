package model

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo/v4"
)

//Employee model
type Employee struct {
	gorm.Model
	Name    string `json:"name"`
	Surname string `json:"surname"`
	Salary  int    `json:"salary"`
}

var Db *gorm.DB
var err error

//InitialMigration opens the connection to the database and creates the user table if it doesn't exist
func InitialMigration() {
	Db, err = gorm.Open("sqlite3", "company.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to database")
	}

	Db.AutoMigrate(&Employee{})
}

func ShowEmployees(c echo.Context) error {

	var employees []Employee
	Db.Find(&employees)

	return c.JSON(http.StatusOK, employees)
}

func ShowEmployee(c echo.Context) error {
	id := c.Param("id")
	var employee = Employee{}

	Db.Where("ID = ?", id).Find(&employee)

	i := int(employee.ID)

	if i == 0 {
		return c.String(http.StatusNotFound, "no employee with specified id")
	}

	return c.JSON(http.StatusOK, &employee)

}

func AddEmployee(c echo.Context) error {

	name := c.FormValue("name")
	surname := c.FormValue("surname")
	salary, ok := strconv.Atoi(c.FormValue("salary"))

	if ok != nil {
		return c.String(http.StatusBadRequest, "Error in form data: Salary must contain only integers")
	}

	employee := Employee{Name: name, Surname: surname, Salary: salary}

	Db.Create(&employee)

	return c.JSON(http.StatusOK, &employee)
}

func RemoveEmployee(c echo.Context) error {
	id := c.Param("id")
	employee := Employee{}

	Db.Where("ID = ?", id).Find(&employee)

	i := int(employee.ID)

	if i == 0 {
		return c.String(http.StatusNotFound, "no employee with specified id")
	}

	Db.Delete(&employee)

	return c.JSON(http.StatusOK, &employee)
}

func UpdateEmployee(c echo.Context) error {
	id := c.Param("id")
	employee := Employee{}
	var ok = err

	Db.Where("id = ?", id).Find(&employee)

	i := int(employee.ID)

	if i == 0 {
		return c.String(http.StatusNotFound, "no employee with specified id")
	}

	employee.Name = c.FormValue("name")
	employee.Surname = c.FormValue("surname")
	employee.Salary, ok = strconv.Atoi(c.FormValue("salary"))

	if ok != nil {
		return c.String(http.StatusBadRequest, "Error in form data: Salary must contain only integers")
	}

	Db.Save(&employee)

	return c.JSON(http.StatusOK, &employee)
}
