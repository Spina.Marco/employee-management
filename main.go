package main

import (
	"employee-management/model"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()
	admin := e.Group("/admin")

	admin.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		if username == "admin" && password == "password" {
			return true, nil
		}
		return false, nil
	}))

	admin.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `[${time_rfc3339}]  ${status}  ${method} ${host}${path} ${latency_human}` + "\n",
	}))

	e.Use(middleware.CORS())

	model.InitialMigration()

	e.GET("/employees", model.ShowEmployees)
	e.GET("/employees/:id", model.ShowEmployee)
	admin.POST("/employees", model.AddEmployee)
	admin.DELETE("/employees/:id", model.RemoveEmployee)
	admin.PUT("/employees/:id", model.UpdateEmployee)

	defer model.Db.Close()

	e.Logger.Fatal(e.Start(":8000"))

}
